import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: {
            admin: {
                'loggedIn': false,
                'correctLogInDetails': true,
                'name': '' 
            }
        },
        mutations: {
            isLoggedInRequest(state, httpObject) {
                console.log(httpObject)
                fetch(httpObject.url)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    console.log(data)
                    if (httpObject.username.trim() === data.name.trim() &&
                    httpObject.password.trim() === data.password.trim()) {
                        state.admin.name = data.name.trim();
                        this.$router.push('/cms-admin/dashboard');
                    } else {
                        state.admin.locorrectLogInDetailsggedIn = false;
                        state.admin.loggedIn = true;
                    }
                }).catch(error => {
                    state.admin.correctLogInDetails = false;
                    state.admin.loggedIn = true;
                    console.error('fetch API error: ', error);
                });
            }
        },
        actions: {},
        getters: {
            isLoggedIn (state) {
                return  state.admin.correctLogInDetails;
            },
            getName (state) {
                return  state.admin.name;
            }
        },
    })
}

export default createStore;