# README #

# Set Up #

* To Run this Project first clone from master branch on by the below command
* 'git clone https://StephenFowles-white@bitbucket.org/StephenFowles-white/cms.git'
* Once cloned install all depenencies with git init
* Then to set up the local server run 'npm run dev'


### Coding Standards ###

* The Setup of each project goes like below 

    |- .nuxt
    |- assets -- |- images
                 |- js
                 |- styles (this is optional)
    |- components ( holds all .vue reusable components )
    |- middleware ( this will be soon updated)
    |- pages |- .index.js (This is always here)
             |- Admin (This folder will hold the CMS system)
    |- plugins 
    |- static (assests should be in the assets folder)
    |- store - this holds the vuex store
    |- test - hold the jest testing script

* Page Set Up

* Each page should hold a structure and spacing simular to below for coding consistency

* A template is needed for all the HTML code
    - There should be no space between each of the template elements
    - Each element should be indeneted in by two (4 spaces) if the element is a child

* In the Script

    - Each page has to have a head function for SEO 
    - Info on this is: https://nuxtjs.org/docs/components-glossary/head/
    - The script should start after one empty space after the temaplate

* Style Tag
    - Use lang scss so it can get varables from tailwind or any scss installs
    - This project uses tailwind but when custom css is needed apply BEM structure
    - When using bem allocate all tailwind properties into the BEM class for spacing
    - make sure there is a space after each class name e.g .body {}
    - make sure there is space after each full class 
    - indent each class is the class is a child of the parent
    - modifyers are on the same indent as the element that is been used


* <template>
*   <Tutorial/>
* </template>

* <script>
  export default {
      head() {
        return {
          title: "Demo header page",
          meta: [
              {
                hid: 'description',
                name: 'description',
                content: 'My custom description'
              }
            ]
        }
      }
  }
* </script>

*<style lang="scss">
.body {
    css ....
}

    .body__element {
        css ....
    }

        .body__element {
            css ....
        }

        .body__element--modifer {
            css ....
        }

    .body__element {

    }
</style>*

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact